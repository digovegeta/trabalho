/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IFabricaPlugins;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/**
 *
 * @author digo_
 */
public interface ISyntaxHighlighter {
    
    public abstract JScrollPane creatEditor(String code);
    public abstract JTextArea getTextArea();

}
