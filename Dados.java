
import IFabricaPlugins.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author digo_
 */
public class Dados {
    
    private IBuilder builder;
    private ISyntaxHighlighter shl;
    private File fileCode = null;
    private String code = ""; 
    private String extensionFileCode = "";
    private String[] listPlugins = {};
    private ArrayList<String> extensionSupports = new ArrayList<String>();
    private ArrayList<IFabricaPlugins> plugins = new ArrayList<IFabricaPlugins>();
    private File folderPlugins = null;
    private URL[] jars = null;
    private URLClassLoader ucl= null;
    private IFabricaPlugins pluginInUser = null;
    private boolean fileReady; 
    private boolean pluginReady; 
    
    public Dados(){
        
    }
    public void setPlugins(JTextField textField){
        File folder = null;
        folder = new File(textField.getText());
        if(textField.getText().equals("")){
            Window.alert("Atenção", "Tentar novamente","Informe um endereço valido");
            pluginReady = false;
            return;
        }
        if(textField.getText().contains(".")){
            Window.alert("Atenção", "Tentar novamente","Informe uma pasta");
            pluginReady = false;
            return;
        }
        if(folder.exists()){
            folderPlugins = folder;
            listPlugins = folderPlugins.list();
            if(listPlugins.length != 0){
                try {
                    refreshPlugins();
                } catch (Exception ex) {
                }
                Window.alert("Plugins", "OK"," Plugins carregados");
                pluginReady = true;
                return;
            }else{
                folderPlugins = null;
                Window.alert("Falta de Plugins", "Recarregar","O caminho indicado não existe plugins!"); 
                pluginReady = false;
                return;
            }
        }else{
            Window.alert("ERRO!", "Tentar novamente", "Pasta não encontrada");
            pluginReady = false;
            return;
        }
    }
    public void setFile(JTextField textField){
        String directory;
        directory = textField.getText();
        File directoryFile = new File(directory);
        if(directory.contains(".") && directoryFile.exists()){
            fileCode = directoryFile;
            extensionFileCode = directory.split("\\.")[1];
            try {
                setCode();
            }catch (Exception ex){                
            }
            fileReady = true;
            return;
        }else{
            Window.alert( "ERRO!", "Tentar novamente","Arquivo não foi encontrado");
            fileReady = false;
            return;
        }
    }
    
    public boolean isFileReady(){
        return fileReady;
    }
    public boolean isPluginReady(){
        return pluginReady;
    }
    public boolean isDataValid(){
        boolean isValidExtension = !extensionFileCode.equals("");
        boolean isValidFolderPlugins = folderPlugins != null;
        if(isValidExtension && isValidFolderPlugins){
            if(plugins.size() != 0 && fileCode.exists() ){
                return CarregarPligun();
            }
        }
        return false;
    }
    public IBuilder getBuilder(){
        return builder;
    }
    public ISyntaxHighlighter getSHL(){
        return shl;
    }
    public IFabricaPlugins getIPlugin(){
        return pluginInUser;
    }
    public File getFileCode(){
        return fileCode;
    }
    public String getCode(){
        return code;
    }
    public String [] getListPlugins(){
        return listPlugins;
    }
               
    private void refreshPlugins() throws Exception{
        plugins = new ArrayList<IFabricaPlugins>();
        extensionSupports = new ArrayList<String>();
        jars = new URL[listPlugins.length];
        for(int i = 0; i < listPlugins.length; i++){
            jars[i] = (new File(folderPlugins.getPath() + "\\" + listPlugins[i])).toURL();
        }
        ucl = new URLClassLoader(jars);
        IFabricaPlugins ip = null;
        String pluginName = "";
        for(int i = 0; i < listPlugins.length; i++){
            pluginName = listPlugins[i].split("\\.")[0];
            ip = (IFabricaPlugins) Class.forName(pluginName + "." +  pluginName, true, ucl).newInstance();
            plugins.add(ip);
            extensionSupports.add(plugins.get(i).supportedExtensions());            
        }        
    }
    private boolean CarregarPligun(){
        pluginInUser = null;
        if(listPlugins.length==0){
             Window.alert("Atenção", "Carregar", "Sem Plugins");
             return false;
        }
        for(int i = 0; i <listPlugins.length;i++){
            if(extensionSupports.get(i).equals(extensionFileCode)){
                pluginInUser = plugins.get(i);
                return true;
            }
        }
        Window.alert("Atenção", "Carregar outro", "Sem suporte ao arquivo");
        return false;
    }
    private void setCode()throws Exception{
        String code = "";
        FileReader readFile = new FileReader(fileCode);
        BufferedReader readFileBuf = new BufferedReader(readFile);
        String line = readFileBuf.readLine();
        while (line != null) {
            code +=  line + "\n";
            line =  readFileBuf.readLine();
        }
        readFile.close();
        this.code = code;
    }
    public void saveCodeFile(){ 
        this.code = shl.getTextArea().getText();
        try {
            FileWriter fileW = new FileWriter(fileCode.getPath());            
            PrintWriter filePW = new PrintWriter(fileW);
            filePW.printf(code);
            fileW.close();
        } catch (Exception e) {
        }
    }
    public void creatPlugins(){
        builder = pluginInUser.creatBuilder();
        shl = pluginInUser.creatSyntaxHighlighter();
    }
}
