/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PluginCPP;
import IFabricaPlugins.*;
        
/**
 *
 * @author digo_
 */
public class PluginCPP implements IFabricaPlugins{
    
    @Override
    public IBuilder creatBuilder(){
        return new Builder();
    }
    @Override
    public ISyntaxHighlighter creatSyntaxHighlighter(){
        return new SyntaxHighlighterCPlus();
    }    

    @Override
    public String supportedExtensions() {
        return "cpp";
    }
}
