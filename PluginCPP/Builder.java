/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PluginCPP;
import IFabricaPlugins.IBuilder;
import java.awt.BorderLayout;
import java.awt.TextArea;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.JFrame;
import static javax.swing.JFrame.HIDE_ON_CLOSE;
import javax.swing.JPanel;
/**
 *
 * @author digo_
 */
public class Builder implements IBuilder{
      @Override
    public String compile(File file) {
        Process processGPP = null;
//        Process processEXE = null;
        Runtime run = Runtime.getRuntime();        

        String exeDir = file.getPath().replace(".cpp",".exe");
        String cmdGPP = "g++ " + file.getPath() + " -o " + exeDir;
        try {
            processGPP = run.exec(cmdGPP);
    //        processEXE = run.exec(exeDir);            
            processGPP.waitFor();
        } catch (IOException ex) {
        } catch (InterruptedException ex) {
        }
       
        return returnProcess(processGPP);
        
}
    private String returnProcess(Process process){
        BufferedReader buf = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        String out = "";
        try {
            while ((line=buf.readLine())!=null) {
                out += line + "\n";
            }
        } catch (IOException ex) {
        }
        return out;
    }
}
