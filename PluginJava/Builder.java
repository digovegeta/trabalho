/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PluginJava;
import IFabricaPlugins.IBuilder;
import java.awt.BorderLayout;
import java.awt.TextArea;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.JFrame;
import static javax.swing.JFrame.HIDE_ON_CLOSE;
import javax.swing.JPanel;
/**
 *
 * @author digo_
 */
public class Builder implements  IBuilder {
    
    @Override
    public String compile(File file) {
        Process processoJavac = null;
        //Process processoJava = null;
        Runtime run = Runtime.getRuntime();
        String cmdJavac = "javac -cp . " + file.getPath();
        //String cmdJava =  "java "+ file.getName().split("\\.")[0];
        File directory = new File(file.getPath().substring(0 , file.getPath().length()-(file.getName().length())));

        try {
            processoJavac = run.exec(cmdJavac);
          //  processoJava = run.exec(cmdJava, null, directory);            
            processoJavac.waitFor();
        } catch (IOException ex) {
        } catch (InterruptedException ex) {
        }

        return returnProcess(processoJavac);
        
    }
    private String returnProcess(Process process){
        BufferedReader buf = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        String out = "";
        try {
            while ((line=buf.readLine())!=null) {
                out += line + "\n";
            }
        } catch (IOException ex) {
        }
        return out;
    }
}