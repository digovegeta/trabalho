/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PluginJava;
import IFabricaPlugins.*;

/**
 *
 * @author digo_
 */
public class PluginJava implements IFabricaPlugins{
    
    @Override
    public IBuilder creatBuilder(){
        return new Builder();
    } 
    
    @Override
    public ISyntaxHighlighter creatSyntaxHighlighter(){
        return new SyntaxHighlighterJava();
    }
    
     @Override
    public String supportedExtensions() {
        return "java";
    }
}
