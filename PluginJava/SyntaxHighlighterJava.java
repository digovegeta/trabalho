/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PluginJava;
import IFabricaPlugins.ISyntaxHighlighter;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;



/**
 *
 * @author digo_
 */
public class SyntaxHighlighterJava extends JFrame implements ISyntaxHighlighter{
private RSyntaxTextArea textArea;
    private RTextScrollPane sp;
    
    @Override
    public JScrollPane creatEditor(String code) {
        textArea = new RSyntaxTextArea(code, 20, 60);
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS);
        textArea.setCodeFoldingEnabled(true);
        sp = new RTextScrollPane(textArea);
        return sp;
    }  
    @Override
    public JTextArea getTextArea() {
        return textArea;
    }
}
