

import IFabricaPlugins.IBuilder;
import IFabricaPlugins.ISyntaxHighlighter;
import java.awt.BorderLayout;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author digo_
 */
public class Window {
    
    private JTextArea textArea;
    private JFrame frame;
    private Dados dados = new Dados();
    private JMenu menuPlugins = new JMenu("Plugins");
    private JMenuBar menuBar;
    private JPanel cp = null;
    private JScrollPane sp = null;
    private JButton bCompile = new JButton("Compile");
        
    public Window(){
        this.frame = new JFrame();
        cp = new JPanel(new BorderLayout());
        textArea = new JTextArea(20, 60);
        sp = new JScrollPane(textArea);
        menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem bUpPlugins = new JMenuItem("Carregar Plugins");
        JMenuItem bUpCodeFile = new JMenuItem("Abrir codígo");
        JMenuItem bExitAction = new JMenuItem("Exit");
        
        bCompile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt){
                dados.saveCodeFile();
                String str = dados.getIPlugin().creatBuilder().compile(dados.getFileCode());
                returnCompile(str, "Retorno da Compilação");
            }
        });
        bUpCodeFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt){
                windowFile();
            }
        });
        bUpPlugins.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                windowPlugins();
            }
        });
        bExitAction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                frame.setVisible(false);
                System.exit(0);
            }
        });
        
        cp.add(sp);
        menuBar.add(fileMenu);
        fileMenu.add(bUpPlugins);
        fileMenu.add(bUpCodeFile);
        fileMenu.addSeparator();
        fileMenu.add(bExitAction);
        frame.setJMenuBar(menuBar);
        frame.setContentPane(cp);
        frame.setTitle("Editex");
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    
    public static void alert(String titulo, String botao, String mensagem){
        String[] options = {botao};
        JOptionPane.showOptionDialog(null, mensagem, titulo, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    }   
    private void refreshFrame(){
        frame.repaint();
        frame.validate();
    }
    private void addMenuPlugins(){
        String [] listPlugins = dados.getListPlugins();
        menuBar.remove(menuPlugins);
        menuPlugins = new JMenu("Plugins");
        for(int i = 0; i < listPlugins.length; i++){
            JMenuItem menuItem = new JMenuItem(listPlugins[i].replace(".jar",""));
            menuPlugins.add(menuItem);      
        }
        if(listPlugins.length != 0){
            menuBar.add(menuPlugins);
        }
        frame.setJMenuBar(menuBar);
        refreshFrame();
    }
    
    private void windowFile(){
        JFrame window = new JFrame();
        JTextField textField = new JTextField(300);
        JMenuBar menuBar = new JMenuBar();
        window.setJMenuBar(menuBar);
        JButton button = new JButton("Carregar arquivo");
        menuBar.add(button);
        button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dados.setFile(textField);
                if(dados.isFileReady()){
                    validatesData();
                    window.setVisible(false);
                }
            }
        });
        window.add(textField);
        window.setTitle("Abrir arquivo");
        window.setSize(300,100);
        window.setDefaultCloseOperation(window.HIDE_ON_CLOSE);
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }
    private void windowPlugins(){
        JFrame window = new JFrame();
        JTextField textField = new JTextField(300);
        JMenuBar menuBar = new JMenuBar();
        window.setJMenuBar(menuBar);
      
        JButton button = new JButton("Carregar plugins");
        menuBar.add(button);
        button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dados.setPlugins(textField);
                if(dados.isPluginReady()){
                    window.setVisible(false);
                    addMenuPlugins();
                    validatesData();
                }
            }
        });
        window.add(textField);
        window.setTitle("Carregar plugins");
        window.setSize(300,100);
        window.setDefaultCloseOperation(window.HIDE_ON_CLOSE);
        window.setLocationRelativeTo(null);
        window.setVisible(true); 
    }
    
    private void pluginUse(){
        dados.creatPlugins();
        upDateSP(dados.getSHL().creatEditor(dados.getCode()));
        textArea = dados.getSHL().getTextArea();
    }
    private void validatesData(){
        if(dados.isDataValid()){
            pluginUse();
            menuBar.remove(bCompile);
            menuBar.add(bCompile);
        }else{
            menuBar.remove(bCompile);
            upDateSP(new JScrollPane(new JTextArea("")));
        }
        refreshFrame();
    }
    private void upDateSP(JScrollPane sp){
        cp.remove(this.sp);
        frame.remove(cp);
        this.sp = sp;
        cp.add(sp);
        frame.setContentPane(cp);
        refreshFrame();
    }
    private void returnCompile(String str, String guia){
        JFrame window = new JFrame();
        JPanel painel = new JPanel(new BorderLayout());
        JTextArea textArea = new JTextArea(str, 20, 60);
        painel.add(textArea);
        window.setContentPane(painel);
        window.setTitle(guia);
        window.setDefaultCloseOperation(frame.HIDE_ON_CLOSE);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }
    
}
